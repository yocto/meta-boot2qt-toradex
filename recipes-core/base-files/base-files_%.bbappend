do_install:append() {
    # add toradex specific mount point
    cat >> ${D}${sysconfdir}/fstab <<EOF
/dev/boot-part       /boot                auto       noatime,noauto        0  0
EOF
}
