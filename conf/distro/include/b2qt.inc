# include file from meta-toradex-nxp
include conf/machine/include/${MACHINE}.inc
# include file from meta-boot2qt
include conf/distro/include/imx.inc
MACHINE_FAMILY = "toradex"

IMAGE_CLASSES += "image_type_tezi_b2qt"

QBSP_IMAGE_CONTENT += "\
    ${IMAGE_LINK_NAME}.tezi.tar \
"

DISTROOVERRIDES .= ":tdx"

BBMASK += "\
    meta-toradex-nxp/backports \
    meta-toradex-nxp/recipes-bsp/u-boot/u-boot-tools_%.bbappend \
"

DEPLOY_CONF_NAME = "Toradex ${MACHINE_NAME}"
DEPLOY_CONF_NAME:apalis-imx6 = "Toradex Apalis iMX6"
DEPLOY_CONF_NAME:apalis-imx8 = "Toradex Apalis iMX8"
DEPLOY_CONF_NAME:colibri-imx6 = "Toradex Colibri iMX6"
DEPLOY_CONF_NAME:colibri-imx6ull = "Toradex Colibri iMX6 ULL"
DEPLOY_CONF_NAME:colibri-imx6ull-emmc = "Toradex Colibri iMX6 ULL eMMC"
DEPLOY_CONF_NAME:colibri-imx7 = "Toradex Colibri iMX7"
DEPLOY_CONF_NAME:colibri-imx7-emmc = "Toradex Colibri iMX7 eMMC"
DEPLOY_CONF_NAME:colibri-imx8x = "Toradex Colibri iMX8X"
DEPLOY_CONF_NAME:verdin-imx8mm = "Toradex Verdin iMX8M Mini"
DEPLOY_CONF_NAME:verdin-imx8mp = "Toradex Verdin iMX8M Plus"

# Suitable DRI device
DRI_DEVICE:apalis-imx6 = "card0"
DRI_DEVICE:colibri-imx6 = "card1"
DRI_DEVICE:colibri-imx7-emmc = "card0"
DRI_DEVICE:apalis-imx8 = "card1"
DRI_DEVICE:apalis-imx8x = "card0"
DRI_DEVICE:colibri-imx8x = "card1"
DRI_DEVICE:colibri-imx8x-v10b = "card1"
DRI_DEVICE:verdin-imx8mm = "card0"
DRI_DEVICE:verdin-imx8mp = "card0"

# remove support for 256MB version colibri-imx6ull in tezi image, since image will be too large for it.
TORADEX_PRODUCT_IDS:remove:colibri-imx6ull = "0036"
